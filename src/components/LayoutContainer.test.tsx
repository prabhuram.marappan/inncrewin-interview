import React from 'react';
import { render, fireEvent } from '../../test/test-utils';

import LayoutContainer from './LayoutContainer';

it('can browse to the about page', async () => {
  // For `LayoutContainer`, you should be able to render the layout container, 
  // followed by navigating to the About page.
  const { history, container, getByText  } = render(<LayoutContainer />);
  expect(container.querySelector('div')).toBeTruthy();
  fireEvent.click(getByText('Go to about'));
  expect(history.location.pathname).toBe('/about');
});
