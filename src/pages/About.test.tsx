import React from 'react';
import { render, fireEvent } from '../../test/test-utils';

import About from './About';

it('renders <About /> page', () => {
  // You should be able to verify the About page rendered correctly.
  const { getByText, getByRole, container  } = render(<About />);
  const h1 = getByRole('heading', {level: 1});
  expect(h1.innerHTML).toBe('About Page');
  expect(getByText("Increment")).toBeTruthy();
  expect(container.querySelector('p').innerHTML).toBe('Current Count: 0');
});

it('clicks button and fires increment counter', () => {
  // You should be able to click the increment button and verify the count.
  const { getByText, getByRole, container  } = render(<About />);
  const btn = getByText("Increment");
  expect(container.querySelector('p').innerHTML).toBe('Current Count: 0');
  expect(btn).toBeTruthy();
  fireEvent.click(btn);
  expect(container.querySelector('p').innerHTML).toBe('Current Count: 1');
});
