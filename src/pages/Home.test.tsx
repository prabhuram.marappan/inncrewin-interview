import React from 'react';
import { render } from '../../test/test-utils';

import Home from './Home';

it('renders <Home /> page', () => {
  const { getByText, getByRole  } = render(<Home />);
  const h1 = getByRole('heading', {level: 1});
  expect(h1.innerHTML).toBe('Welcome!');
  expect(getByText("Go to about").href).toBe("http://localhost/about");
});
